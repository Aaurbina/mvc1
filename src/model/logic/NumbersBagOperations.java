package model.logic;

import java.util.Iterator;

import model.data_structures.NumbersBag;

public class NumbersBagOperations <T extends Number> {

	
	
	public double computeMean(NumbersBag bag){
		double mean = 0;
		int length = 0;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				mean += iter.next();
				length++;
			}
			if( length > 0) mean = mean / length;
		}
		return mean;
	}
	
	
	public int getMax(NumbersBag bag){
		int max = Integer.MIN_VALUE;
	    int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( max < value){
					max = value;
				}
			}
			
		}
		return max;
	}
	
	public T getMin(NumbersBag bag){
		T min = 0;
	    T value;
		if(bag != null){
			Iterator<T> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( min > value){
					min = value;
				}
			}
			
		}
		return min;
	}
		
	public T getLastNum(NumbersBag<T> bag){
		T lastNum = null;
		if(bag != null){
			Iterator<T> iter = bag.getIterator();
			while(iter.hasNext()){
				lastNum = iter.next();
			}
		}
		return lastNum;
	}
}
