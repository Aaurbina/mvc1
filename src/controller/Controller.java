package controller;

import java.util.ArrayList;

import model.data_structures.NumbersBag;
import model.logic.NumbersBagOperations;

public class Controller <T extends Number>{

	private static NumbersBagOperations model = new NumbersBagOperations();
	private static NumbersBag bag;
	
	
	public static NumbersBag createBag(ArrayList<Integer> values){
         return new NumbersBag(values);		
	}
	
	
	public static double getMean(NumbersBag bag){
		return model.computeMean(bag);
	}
	
	public static double getMax(NumbersBag bag){
		return model.getMax(bag);
	}
	
	public static Number getMin(NumbersBag bag){
		return model.getMin(bag);
	}
	
	public static Number getLastNum(NumbersBag bag){
		return model.getLastNum(bag);
	}
}
